import "./style.css";

// 1. Import modules.
import { createPublicClient, http, getContract, formatUnits, createWalletClient, custom, parseUnits, parseAbiItem, getAddress } from "viem";
import { goerli } from "viem/chains";
import { UNI } from "./abi/UNI";


// 2. Set up your client with desired chain & transport.
const publicClient = createPublicClient({
    chain: goerli,
    transport: http(),
});

const [account] = await window.ethereum.request({ method: "eth_requestAccounts" });
const walletClient = createWalletClient({
    account,
    chain: goerli,
    transport: custom(window.ethereum),
});

const uniContract = getContract({
    address: "0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984", // UNI address
    abi: UNI,
    publicClient,
    walletClient,
});

// 3. Consume an action!
const decimals = await uniContract.read.decimals();
const symbol = await uniContract.read.symbol();
const blockNumber = await publicClient.getBlockNumber();
const contractName = await uniContract.read.name();
const totalSupply = (formatUnits(await uniContract.read.totalSupply(), decimals));
const myAddress = account;
let balanceOf = (formatUnits(await uniContract.read.balanceOf([myAddress]), decimals));;

// await uniContract.write.transfer(["0xc2ba723430706885144a37b2EC74feb82A0C1333", 1n]);

document.querySelector("#app").innerHTML = `
  <div>
    <p>Current block is <span id="blockNumber"> ${blockNumber}</span></p>
    <h1>Token ${symbol}</h1>
    <p>Name:${contractName}</p>
    <p>Address: <a href="https://goerli.etherscan.io/token/0x1f9840a85d5af5bf1d1762f925bdaddc4201f984">${uniContract.address}</a></p>
    <p>Total supply: ${totalSupply}</p>
    <p>Balance of ${myAddress}: <span id="balance">${balanceOf}</span></p>
    <hr/>
    <form>
      <label for="amountInput">amount:</label>
      <input type="text" id="amountInput" name="amount">
      <button id="max">Max</button></br></br>
      <label for="recipientInput">recipient:</label>
      <input type="text" id="recipientInput" name="recipient">
      <button id="send">Send </button>
    </form>
    <span id="transactionSpan"></span>
  </div>
`;

document.querySelector("#max").addEventListener("click", (event) => {
    event.preventDefault();
    document.querySelector("#amountInput").value = balanceOf;
});

document.querySelector("#send").addEventListener("click", async(event) => {
    event.preventDefault();
    const amount = parseUnits(document.querySelector("#amountInput").value, decimals);
    const recipient = document.querySelector("#recipientInput").value;
    const hash = await uniContract.write.transfer([recipient, amount]);
    document.querySelector("#transactionSpan").innerHTML = `Waiting for tx <a href="https://goerli.etherscan.io/tx/${hash}">${hash}</a>`;
    const transaction = await publicClient.waitForTransactionReceipt({ hash: `${hash}` });
    if (transaction.status == "success") {
        document.querySelector("#transactionSpan").innerHTML = `Transaction <a href="https://goerli.etherscan.io/tx/${hash}">${hash}</a> confirmed!`;
    } else {
        document.querySelector("#transactionSpan").innerHTML = `Transaction <a href="https://goerli.etherscan.io/tx/${hash}">${hash}</a> failed!`;
    };
});

publicClient.watchBlockNumber({
    onBlockNumber: async() => {
        const logs = await publicClient.getLogs({
            address: uniContract.address,
            event: parseAbiItem("event Transfer(address indexed from, address indexed to, uint256 value)"),
        });

        const myTransfers = logs.filter((log) => log.args.from == getAddress(account) || log.args.to == getAddress(account));

        if (myTransfers.length > 0) {
            balanceOf = await uniContract.read.balanceOf([account]);
            console.log(`New balance ${balanceOf}`);
            document.querySelector("#balance").innerHTML = formatUnits(balanceOf, decimals);
        }
    }
})

console.log(goerli);

const unwatch = publicClient.watchBlockNumber({ onBlockNumber: blockNumber => { document.querySelector("#blockNumber").innerHTML = `
   ${blockNumber}
`; } })