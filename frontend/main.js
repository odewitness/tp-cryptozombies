import "./style.css";

// 1. Import modules.
import { createPublicClient, http, getContract, createWalletClient, custom, parseEther } from "viem";
import { goerli } from "viem/chains";
import { CryptoZombies } from "./abi/CryptoZombies";

// 2. Set up your client with desired chain & transport.
const publicClient = createPublicClient({
    chain: goerli,
    transport: http(),
});

const [account] = await window.ethereum.request({ method: "eth_requestAccounts" });
const walletClient = createWalletClient({
    account,
    chain: goerli,
    transport: custom(window.ethereum),
});

const zombieContract = getContract({
    address: "0x0d97c666526F0460e2374ba741281c31B5ee7F52", // UNI address
    abi: CryptoZombies,
    publicClient,
    walletClient,
});

// 3. Consume an action!
const blockNumber = await publicClient.getBlockNumber();
const ownerAddress = await zombieContract.read.owner();
const balanceOfOwner = await zombieContract.read.balanceOf([account]);
const ZombiesByOwner = await zombieContract.read.getZombiesByOwner([account]);

const zombieIds = [0, 1, 2]; // Add more IDs if needed
const zombieInfos = [];

for (const zombieId of zombieIds) {
    const info = await zombieContract.read.zombies([zombieId]);
    zombieInfos.push(info);
}

document.querySelector("#app").innerHTML = `
  <div>
  <h1> -- My Zombies crew 🧟-- </h1>
  <h2> My infos  </h2>    
    <p>Current block is ${blockNumber}</p>
    <p>Your adress is ${ownerAddress}</p>
    <p>Your balance is ${balanceOfOwner}</p>
    <p>You're the owner of the zombie with the id n°${ZombiesByOwner}</p>

  <h2> My Zombie factory </h2>
  <div class="gallery-container">
  <div class="zombie-container">
  <h3>Create a zombie</h3>    
    <form>
      <label for="nameMyZombie">Name of your zombie:</label>
      <input type="text" id="nameMyZombie" name="name">
      <button id="createNewZ"> Create a random Zombie </button>
    </form>
    <span id="newZombieSpan"></span>
    </div> 
  <div class="zombie-container">  
  <h3>Level up a zombie </h3>
  <button id="levelUpZombie"> Level up your first zombie </button>
  </div>
  </div> 

  <h2> Zombies gallery </h2>
    <div class="gallery-container">
    ${zombieInfos.map((info, index) => `
      <div class="zombie-container">
        <h3>Zombie ${index + 1}</h3>
        <ul>
          <li>Name: ${info[0]}</li>
          <li>DNA: ${info[1]}</li>
          <li>DNA: ${info[2]}</li>
          <li>Level: ${info[3]}</li>
          <li>Wins: ${info[4]}</li>
          <li>Losses: ${info[5]}</li>
          <li>Ready Time: ${info[6]}</li>
        </ul>
      </div>
    `).join('')}
  </div>

  <h2> Your zombie gallery </h2>
    <div class="gallery-container">
      <div class="zombie-container">
       
    </div>
  </div>
`;

document.querySelector("#createNewZ").addEventListener("click", async(event) => {
    event.preventDefault();
    const hash = await zombieContract.write.createRandomZombie([document.querySelector("#nameMyZombie").value]);
    document.querySelector("#newZombieSpan").innerHTML = `Waiting for new zombie`;
    const transaction = await publicClient.waitForTransactionReceipt({ hash: `${hash}` });
    if (transaction.status == "success") {
        document.querySelector("#newZombieSpan").innerHTML = `Transaction confirmed!`;
    } else {
        document.querySelector("#newZombieSpan").innerHTML = `Transaction failed!`;
    };
});

document.querySelector("#levelUpZombie").addEventListener("click", async () => {
    try {
        if (balanceOfOwner === 0) {
            alert("You need to create a zombie before leveling up!");
            return;
        }

        const zombieIdToLevelUp = zombieIds[0];

        const levelUpTx = await walletClient.writeContract({
            address: zombieContract.address,
            abi: CryptoZombies,
            functionName: 'levelUp',
            args: [zombieIdToLevelUp],
            value: parseEther('0.001')
        });

        zombieInfos[zombieIdToLevelUp] = await zombieContract.read.zombies([zombieIdToLevelUp]);

    } catch (error) {
        console.error("Error while leveling up the zombie", error);
        alert("Error while leveling up the zombie");
    }
});